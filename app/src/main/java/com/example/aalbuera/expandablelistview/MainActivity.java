package com.example.aalbuera.expandablelistview;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int FIRST_LEVEL_COUNT = 6;
    public static final int SECOND_LEVEL_COUNT = 4;
    public static final int THIRD_LEVEL_COUNT = 20;
    private ExpandableListView expandableListView;

    public List<String> firstList = new ArrayList<>();



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expandableListView = (ExpandableListView) findViewById(R.id.mainList);

        firstList.add("Sample1");
        firstList.add("Sample2");
        firstList.add("Sample3");
        firstList.add("Sample4");
        firstList.add("Sample5");

        expandableListView.setAdapter(new ParentLevel(this, firstList));


    }

    public class ParentLevel extends BaseExpandableListAdapter {

        private Context context;
        private final LayoutInflater mInflater;
        private final ViewBinderHelper binderHelper;
        private List<String> objects;
        private ParentLevel.ViewHolder parentViewHolder;
        SecondLevelAdapter secondLevelAdapter;


        public ParentLevel(Context context, List<String> objects) {
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            this.binderHelper = new ViewBinderHelper();
            this.objects = objects;

        }

        @Override
        public Object getChild(int arg0, int arg1) {
            return arg1;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(MainActivity.this);
            secondLevelAdapter = new SecondLevelAdapter(context, firstList, ParentLevel.this);
            secondLevelELV.setAdapter(secondLevelAdapter);
            secondLevelELV.setGroupIndicator(null);
            return secondLevelELV;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return firstList.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public int getGroupCount() {
            return objects.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }


        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


            final ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_first, parent, false);

                holder = new ViewHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.text);
                holder.deleteView = convertView.findViewById(R.id.delete_layout);
                holder.swipeLayout = (SwipeRevealLayout) convertView.findViewById(R.id.swipe_layout);
                binderHelper.bind(holder.swipeLayout, firstList.get(groupPosition));
                binderHelper.setOpenOnlyOne(true);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.textView.setText(firstList.get(groupPosition));
            setParentViewHolder(holder);

            holder.swipeLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
                @Override
                public void onClosed(SwipeRevealLayout view) {

                }

                @Override
                public void onOpened(SwipeRevealLayout view) {



                }

                @Override
                public void onSlide(SwipeRevealLayout view, float slideOffset) {

                    if (!view.isOpened()){
                        Toast.makeText(getApplicationContext(), "CLOSE MO ITO", Toast.LENGTH_SHORT).show();
                        secondLevelAdapter.getSeconViewHolder().swipeLayout.close(true);
                    }

                }
            });

            return convertView;

        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        private String getItem(int position) {
            return objects.get(position);
        }


        public class ViewHolder {
            TextView textView;
            View deleteView;
            SwipeRevealLayout swipeLayout;
        }


        public ParentLevel.ViewHolder getParentViewHolder() {
            return parentViewHolder;
        }

        public void setParentViewHolder(ParentLevel.ViewHolder parentViewHolder) {
            this.parentViewHolder = parentViewHolder;
        }


    }

    public class SecondLevelExpandableListView extends ExpandableListView {

        public SecondLevelExpandableListView(Context context) {
            super(context);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public class SecondLevelAdapter extends BaseExpandableListAdapter {

        private Context context;
        private final LayoutInflater mInflater;
        private final ViewBinderHelper binderHelper;
        private List<String> objects;
        private ParentLevel parentLevel;


        private SecondLevelAdapter.ViewHolder seconViewHolder;


        public SecondLevelAdapter(Context context, List<String> objects, ParentLevel parentLevel) {
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            this.binderHelper = new ViewBinderHelper();
            this.objects = objects;
            this.parentLevel = parentLevel;

        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public int getGroupCount() {
            return firstList.size();
        }

        public ViewHolder getSeconViewHolder() {
            return seconViewHolder;
        }

        public void setSeconViewHolder(ViewHolder seconViewHolder) {
            this.seconViewHolder = seconViewHolder;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent) {

            final ViewHolder holder;


            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_second, parent, false);

                holder = new SecondLevelAdapter.ViewHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.text);
                holder.deleteView = convertView.findViewById(R.id.delete_layout);
                holder.swipeLayout = (SwipeRevealLayout) convertView.findViewById(R.id.swipe_layout);
                binderHelper.bind(holder.swipeLayout, firstList.get(groupPosition));
                binderHelper.setOpenOnlyOne(true);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.textView.setText(firstList.get(groupPosition));
            setSeconViewHolder(holder);


            holder.swipeLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
                @Override
                public void onClosed(SwipeRevealLayout view) {

                }

                @Override
                public void onOpened(SwipeRevealLayout view) {
                    if (parentLevel.getParentViewHolder().swipeLayout.isOpened()){
                        Toast.makeText(getApplicationContext(), "CLOSE MO ITO", Toast.LENGTH_SHORT).show();
                        parentLevel.getParentViewHolder().swipeLayout.close(true);
                    }
                }

                @Override
                public void onSlide(SwipeRevealLayout view, float slideOffset) {

                }
            });


            return convertView;
        }



        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_third, null);
                TextView text = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
                text.setText("THIRD LEVEL");
            }
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return THIRD_LEVEL_COUNT;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public class ViewHolder {
            TextView textView;
            View deleteView;
            SwipeRevealLayout swipeLayout;
        }

    }
}
